#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (c) 2016, Jean-Benoist Leger <jb@leger.tf>
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
# 
#     Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
# 
#     Redistributions in binary form must reproduce the above copyright
#     notice, this list of conditions and the following disclaimer in
#     the documentation and/or other materials provided with the
#     distribution.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


from twisted.words.protocols import irc
from twisted.internet import reactor, protocol
from twisted.python import log

# system imports
import time, sys

import legifrance
import random
import re

import configobj

class MessageLogger:
    
    def log(self, message):
        print message

class MmBot(irc.IRCClient):
    
    def __init__(self,botconf):
        self.botconf = botconf
        self.nickname = self.botconf['nickname']
        self.codes = legifrance.legi('legifrance.conf')
        self.view = {}
        self.view_content = {}
        
    def connectionMade(self):
        irc.IRCClient.connectionMade(self)
        self.logger = MessageLogger()
        self.logger.log("[connected at %s]" % 
                        time.asctime(time.localtime(time.time())))

    def connectionLost(self, reason):
        irc.IRCClient.connectionLost(self, reason)
        self.logger.log("[disconnected at %s]" % 
                        time.asctime(time.localtime(time.time())))

    def signedOn(self):
        """Called when bot has succesfully signed on to server."""
        for c in self.botconf['chans']:
            self.logger.log("[asked to join %s]" % c)
            self.join(c)

    def kickedFrom(self, channel, kicker, message):
        self.logger.log("[kicked from %s by %s at %s]" % 
                ( channel, kicker,
                        time.asctime(time.localtime(time.time())))
                )

    def joined(self, channel):
        """This will get called when the bot joins the channel."""
        self.logger.log("[I have joined %s]" % channel)

    def c_help(self, user):
        rep = u"Quand du texte est rencontré sur un channel, de la forme :\n"
        rep+= u"   [Name Article], [Article du Name], ou [Article de la Name]\n"
        rep+= u"   alors le lien vers legifrance est affiché.\n"
        rep+= u"   Exemple: [CP 323-1], [323-1 du CP] ou [30 de la LCEN].\n"
        rep+= u"De même quand il est rencontré un motif de la forme {Name Article},\n"
        rep+= u"   {Article du Name} ou {Article de la Name}, alors le contenu\n"
        rep+= u"   de l'article est affiché\n"
        rep+= u"   Exemple: {CP 323-1}, {323-1 du CP} ou {30 de la LCEN}.\n"
        rep+= u"Commandes disponnibles en query:\n"
        rep+= u" - help\n"
        rep+= u" - list\n"
        rep+= u" - add (code|texte) <name> <codeid> (e.g. \"add code CP LEGITEXT000006070719\")\n"
        rep+= u" - expire <codename> (e.g. \"expire CP\")\n"
        rep+= u" - reload_legifranceconf\n"
        rep+= u" - reload_botconf\n"
        rep+= u" - join <channel>\n"
        rep+= u"Commandes disponnibles sur les chans:\n"
        rep+= u" - !legifrance help\n"
        rep+= u" - !legifrance list\n"
        rep+= u" - !legifrance add (code|texte) <name> <codeid> (e.g. \"!legifrance add code CP LEGITEXT000006070719\")\n"
        rep+= u" - !legifrance expire <codename> (e.g. \"!legifrance expire CP\")\n"
        rep+= u" - !legifrance reload_legifranceconf\n"
        rep+= u" - !legifrance reload_botconf\n"
        rep+= u" - !legifrance join <channel>\n"
        rep+= u" - !legifrance part\n"
        self.msg(user, rep.encode('utf8'))

    def c_list(self, user):
        rep = "Codes disponnibles :\n"
        for c in self.codes.conf['codeids'].keys():
            rep += ' - %s : %s\n' % (c,self.codes.conf['codeids'][c])
        rep+= "Textes disponnibles :\n"
        for c in self.codes.conf['textids'].keys():
            rep += ' - %s : %s\n' % (c,self.codes.conf['textids'][c])
        self.msg(user, rep)

    def c_add(self, user, what, name, nameid):
        if what == 'code':
            if self.codes.conf['codeids'].has_key(name):
                self.msg(user, "Ce code existe.")
            else:
                if self.codes.conf['textids'].has_key(name):
                    self.msg(user, "Un texte existe sous le meme nom.")
                else:
                    self.codes.conf['codeids'][name]=nameid
                    self.codes.conf.write()
                    self.msg(user, "done")
        if what == 'texte':
            if self.codes.conf['textids'].has_key(name):
                self.msg(user, "Ce texte existe.")
            else:
                if self.codes.conf['codeids'].has_key(name):
                    self.msg(user, "Un code existe sous le meme nom.")
                else:
                    self.codes.conf['textids'][name]=nameid
                    self.codes.conf.write()
                    self.msg(user, "done")

    def c_expire(self, user, name):
        if (not self.codes.conf['codeids'].has_key(name)) and (not self.codes.conf['texteids'].has_key(name)):
            self.msg(user, "Aucun texte et code de ce nom")
        if self.codes.force_ct_reload(codename):
            self.msg(user, "done")
        else:
            self.msg(user, "nothing to do")

    def c_reload_legifranceconf(self, user):
        self.codes.conf.reload()
        self.msg(user, "done")

    def c_reload_botconf(self, user):
        self.botconf.reload()
        self.msg(user, "done")

    def privmsg(self, user, channel, msg):
        """This will get called when the bot receives a message."""
        user = user.split('!', 1)[0]
        self.logger.log("%s: <%s> %s" % (channel, user, msg))
        
        forbot=False
        m=''

        if channel == self.nickname:
            forbot=True
            m=msg

        a=re.match('!legifrance (.*)',msg)
        if a:
            forbot=True
            m=a.groups()[0]

        if forbot:
            # pv or prefixed
            if re.match('help',m):
                self.c_help(user)

            if re.match('list',m):
                self.c_list(user)

            a=re.match('add (code|texte) ([A-Za-z0-9]+) ([A-Za-z0-9]+)', m)
            if a:
                self.c_add(user, a.groups()[0], a.groups()[1], a.groups()[2])

            a=re.match('expire ([A-Za-z0-9]+)', m)
            if a:
                self.c_expire(user,a.groups()[0])

            if re.match('reload_legifranceconf', m):
                self.c_reload_legifranceconf(user)
            
            if re.match('reload_botconf', m):
                self.c_reload_botconf(user)

            a=re.match('join (#[A-Za-z0-9-]+)', m)
            if a:
                self.join(a.groups()[0])

            if re.match('part',m):
                if channel == self.nickname:
                    self.msg(user, 'Cette commande doit être lancé sur un cannal')
                else:
                    self.part(channel)

        else:
            # public
            
            # coeur de metier
            m = msg
            reg = r'(?:'
            reg+= r'(?P<mark1a>(?:\[|\{))(?P<codename1>[A-Za-z0-9]+) (?P<article1>[^ \]]+)(?P<mark1b>(?:\]|\}))'
            reg+= r'|'
            reg+= r'(?P<mark2a>(?:\[|\{))(?P<article2>[^ \]]+) du (?P<codename2>[A-Za-z0-9]+)(?P<mark2b>(?:\]|\}))'
            reg+= r'|'
            reg+= r'(?P<mark3a>(?:\[|\{))(?P<article3>[^ \]]+) de la (?P<codename3>[A-Za-z0-9]+)(?P<mark3b>(?:\]|\}))'
            reg+= r')'
            while True:
                a = re.match('.*?%s'%reg, m)
                if a is None:
                    break
                d=a.groupdict()
                if not d['codename1'] is None:
                    codename = d['codename1']
                    article = d['article1']
                    form = '%s %s' % (codename, article)
                    marks = (d['mark1a'],d['mark1b'])
                if not d['codename2'] is None:
                    codename = d['codename2']
                    article = d['article2']
                    form = '%s du %s' % (article, codename)
                    marks = (d['mark2a'],d['mark2b'])
                if not d['codename3'] is None:
                    codename = d['codename3']
                    article = d['article3']
                    form = '%s de la %s' % (article, codename)
                    marks = (d['mark3a'],d['mark3b'])
                self.logger.log("[match] %s" % form)

                if (marks[0] == '{' and marks[1]=='}') or (marks[0]=='[' and marks[1]==']') :

                    trop_jeune = False

                    if marks[0] == '{':
                        used_view = self.view_content
                    else:
                        used_view = self.view

                    if used_view.has_key( (channel,codename,article) ):
                        if time.time()-used_view[(channel,codename,article)] < int(self.botconf['remain_time']):
                            trop_jeune = True

                    if not trop_jeune:
                        if marks[0] == '[':
                            link = self.codes.get(codename,article)
                            if not link is None:
                                self.msg(channel,'[%s]: %s' % (form, link))
                        else:
                            txt = self.codes.get_content(codename,article)
                            if not txt is None:
                                self.msg(channel,'{%s}' % form)
                                for mline in txt.split('\n'):
                                    self.msg(channel,mline)
                    used_view[(channel,codename,article)] = time.time()
                m= re.sub(reg,'',m,1)


class MmBotFactory(protocol.ClientFactory):

    def __init__(self,botconf):
        self.botconf = botconf
        pass

    def buildProtocol(self, addr):
        p = MmBot(self.botconf)
        p.factory = self
        return p

    def clientConnectionLost(self, connector, reason):
        time.sleep(30)
        connector.connect()

    def clientConnectionFailed(self, connector, reason):
        print "connection failed:", reason
        time.sleep(30)
        connector.connect()


if __name__ == '__main__':

    botconf = configobj.ConfigObj('bot.conf')
    log.startLogging(sys.stdout)
    f = MmBotFactory(botconf)

    reactor.connectTCP(botconf['server_name'], int(botconf['server_port']), f)
    reactor.run()
