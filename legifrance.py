
# Copyright (c) 2016, Jean-Benoist Leger <jb@leger.tf>
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
# 
#     Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
# 
#     Redistributions in binary form must reproduce the above copyright
#     notice, this list of conditions and the following disclaimer in
#     the documentation and/or other materials provided with the
#     distribution.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import requests
import re
import time
import configobj
import html2text
import multiprocessing

def get_articles_from_page(link):
    articles={}
    r = requests.get(link)

    reg = '^.*?<a href="/?(affich[A-Za-z]*Article\.do[^"]*idArticle[^"]*)" title="En savoir plus sur l\'article ([^"]+)"'
    c=r.content

    while True:
        a = re.match(reg, c, re.DOTALL)
        if a is None:
            break
        l1 = 'https://www.legifrance.gouv.fr/'+a.groups()[0]
        l1 = re.sub('&amp;','&',l1)
        l1 = re.sub(';jsessionid=[^\?]*\?','?',l1)
        l1 = re.sub('&dateTexte=[^&]*','',l1)
        articles[a.groups()[1]] = l1
        c = re.sub('href=','',c,1)

    return articles

def get_article_content(link):
    r = requests.get(link)

    reg = '^.*<div class="corpsArt">(.*)</div>.*?Liens relatifs'
    c = r.content.decode('utf8')
    a = re.match(reg, c, re.DOTALL)
    if a:
        content = a.groups()[0]
        h = html2text.HTML2Text()
        h.ignore_links=True
        return h.handle(content).encode('utf8')
    return None

def get_code(codename,codeids):

    if not codename in codeids:
        return None

    codeid = codeids[codename]

    r=requests.get('https://www.legifrance.gouv.fr/affichCode.do?cidTexte='+codeid)

    reg = '^.*?href="(affichCode\.do[^"]*idSectionTA[^"]*)"'
    c=r.content
    links=set()

    while True:
        a = re.match(reg,c,re.DOTALL)
        if a is None:
            break
        l1 = 'https://www.legifrance.gouv.fr/'+a.groups()[0]
        l1 = re.sub('&amp;','&',l1)
        links.add(l1)
        c = re.sub('href=','',c,1)

    articles = {}

    pool = multiprocessing.Pool(processes=25)
    pool_outputs = pool.map(get_articles_from_page,links)
    pool.close()
    pool.join()

    for po in pool_outputs:
        articles.update(po)

    return articles

def get_text(textname, textids):
    if not textname in textids:
        return None

    textid = textids[textname]
    url = 'https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=' + textid
    return get_articles_from_page(url)

class legi:

    def __init__(self,conffile):
        self.cache = {}
        self.conf = configobj.ConfigObj(conffile)

    def get(self,name,article):

        t = time.time()

        if name in self.cache:
            if t - self.cache[name]['timestamp'] > int(self.conf['expire']):
                del self.cache[name]
        if name in self.cache:
            pass
        else:
            if name in self.conf['codeids'].keys():
                articles = get_code(name,self.conf['codeids'])
            else:
                articles = get_text(name,self.conf['textids'])

            if not articles is None:
                self.cache[name] = {'timestamp':t, 'articles':articles}

        if name in self.cache:
            if article in self.cache[name]['articles']:
                return self.cache[name]['articles'][article]
        return None

    def get_content(self,name,article,irc_format=False):
        link = self.get(name,article)
        if link is None:
            return None
        txt = get_article_content(link)
        txt = re.sub('^\n+','',txt)
        txt = re.sub('\n+$','',txt)
        txt = re.sub('\n{3,}','\n\n',txt)
        txt = re.sub('\n\n','\n \n',txt)
        return txt

    def force_ct_reload(self,name):
        if name in self.codes:
            self.codes[name]['timestamp']=0
            return True
        return False
        


    



